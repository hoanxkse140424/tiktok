import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { OutlinedInput } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import InputLabel from '@mui/material/InputLabel';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Controller } from 'react-hook-form';

PasswordField.propTypes = {
    form: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired,

    label: PropTypes.string,
    disabled: PropTypes.bool,
};

function PasswordField(props) {
    const {
        form, name, label, disabled
    } = props
    const { formState } = form
    const hasError =
        formState.errors[name] && formState.touchedFields[name]

    // console.log(formState.errors[name], formState.touchedFields[name]);

    const [showPassword, setShowPassword] = useState(false)
    const toggleShowPassword = () => {
        setShowPassword(x => !x)
    }
    return (



        <FormControl
            sx={{ m: 1.5 }}
            style={{ marginLeft: 1 }}
            fullWidth
            variant="outlined">
            <InputLabel htmlFor={name}>Password</InputLabel>
            <Controller
                name={name}
                control={form.control}
                render={({ field }) =>
                    <OutlinedInput
                        {...field}
                        id={name}
                        type={showPassword ? 'text' : 'password'}

                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={toggleShowPassword}

                                    edge="end"
                                >
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                        label="Password"
                        disabled={disabled}
                        error={hasError}

                    // helperText={formState.errors[name]?.message}
                    />
                }
            />

        </FormControl>




    );
}

export default PasswordField;