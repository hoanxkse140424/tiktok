import { Close } from '@mui/icons-material';
import MenuIcon from '@mui/icons-material/Menu';
import { IconButton } from '@mui/material';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import clsx from 'clsx';
import Login from 'features/Auth/components/Login';
import { useState } from 'react';
// import * as React from 'react';
import { Link, NavLink } from 'react-router-dom';
import styles from './Header.module.scss';

export default function Header() {
    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>

                    {/* size="large"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }}
                    > */}
                    <MenuIcon
                        size="large"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }} />

                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        <Link
                            className={clsx(styles.link)}
                            to="/">
                            Testing Page
                        </Link>
                    </Typography>

                    <NavLink
                        className={clsx(styles.link)}
                        to="/todos">
                        <Button color="inherit">Todos</Button>
                    </NavLink>


                    <NavLink
                        className={clsx(styles.link)}
                        to="/albums">
                        <Button color="inherit">Albums</Button>
                    </NavLink>

                    <Button
                        color="inherit"
                        onClick={handleClickOpen}
                    >Register</Button>
                </Toolbar>
            </AppBar>
            <Dialog
                open={open}
                disableEscapeKeyDown
                onClose={(event, reason) => {
                    if (reason !== 'backdropClick') {
                        handleClose(event, reason);
                    }
                }}
            >
                <IconButton
                    className={clsx(styles.closeButton)}
                    onClick={handleClose}
                >
                    <Close />
                </IconButton>

                <DialogContent>
                    {/* <Register closeDialog={handleClose} /> */}

                    <Login closeDialog={handleClose} />
                </DialogContent>

            </Dialog>
        </Box >
    );
}
