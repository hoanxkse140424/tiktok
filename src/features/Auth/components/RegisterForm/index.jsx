import { yupResolver } from '@hookform/resolvers/yup';
import { LockClockOutlined } from '@mui/icons-material';
import { Avatar, Button, LinearProgress, Typography } from '@mui/material';
import InputField from 'components/form-controls/InputField';
import PasswordField from 'components/form-controls/PasswordField';
import PropTypes from 'prop-types';
import React from 'react';
import { useForm } from 'react-hook-form';
import * as yup from "yup";
// import InputField from '../../../../components/form-controls/InputField';


RegisterForm.propTypes = {
    onSubmit: PropTypes.func,
};

function RegisterForm(props) {
    const schema = yup.object(
        {
            fullName: yup.string()
                .required(
                    'Please enter your fullname'
                )
                .test('should has at least 2 words',
                    'Please enter at least 2 words',
                    (value) => {
                        console.log('Value', value);
                        return value.split(' ').length >= 2
                    })
        }
    )
    const form = useForm({

        defaultValues: {
            fullName: '',
            email: '',
            password: '',
            retypePassword: '',
        },
        resolver: yupResolver(schema)
    })
    const handleSubmit = async (values) => {
        // console.log("Todo form: ", values);
        const { onSubmit } = props;
        if (onSubmit)
            await onSubmit(values)
        form.reset()
    }
    const { isSubmitting } = form.formState
    return (
        <div>
            {isSubmitting && <LinearProgress />}
            <Avatar>
                <LockClockOutlined></LockClockOutlined>
            </Avatar>
            <Typography component="h3" variant="h5">
                Create an account
            </Typography>
            <form onSubmit=
                {form.handleSubmit(handleSubmit)}>

                <InputField
                    name="fullName"
                    label="Full Name"
                    form={form} />
                <InputField
                    name="email"
                    label="Email"
                    form={form} />
                <PasswordField
                    name="password"
                    label="Password"
                    form={form} />
                <PasswordField
                    name="retypePassword"
                    label="Retype Password"
                    form={form} />
                <Button
                    disabled={isSubmitting}
                    variant='contained'
                    color='primary'
                    fullWidth
                    type='submit'
                >
                    Create an account
                </Button>
            </form>
        </div>
    );
}

export default RegisterForm;