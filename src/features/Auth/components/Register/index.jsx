import { register } from 'features/Auth/userSlide';
import { useSnackbar } from 'notistack';
import React from 'react';
import { useDispatch } from 'react-redux';
import RegisterForm from '../RegisterForm';
import PropType from 'prop-types'
Register.propTypes = {
    closeDialog: PropType.func
};

function Register(props) {

    const dispatch = useDispatch()

    const { enqueueSnackbar } = useSnackbar();

    const handleSubmit = async (values) => {
        //auto set username=email
        values.username = values.email
        try {
            const action = register(values)
            const user
                = await dispatch(action).unwrap()

            //close dialog
            const { closeDialog } = props
            if (closeDialog) {
                closeDialog()
            }


            enqueueSnackbar('Register successfully',
                {
                    variant: 'success'
                })
        } catch (error) {
            console.log('Failed to register:', error);
        }
    }
    return (
        <div>
            <RegisterForm onSubmit={handleSubmit} />
        </div>
    );
}

export default Register;