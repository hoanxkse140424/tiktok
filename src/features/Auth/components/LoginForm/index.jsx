import { yupResolver } from '@hookform/resolvers/yup';
import { LockClockOutlined } from '@mui/icons-material';
import { Avatar, Button, LinearProgress, Typography } from '@mui/material';
import InputField from 'components/form-controls/InputField';
import PasswordField from 'components/form-controls/PasswordField';
import PropTypes from 'prop-types';
import React from 'react';
import { useForm } from 'react-hook-form';
import * as yup from "yup";
// import InputField from '../../../../components/form-controls/InputField';


LoginForm.propTypes = {
    onSubmit: PropTypes.func,
};

function LoginForm(props) {
    const schema = yup.object(

    )
    const form = useForm({

        defaultValues: {

            identifier: '',
            password: '',

        },
        resolver: yupResolver(schema)
    })
    const handleSubmit = async (values) => {
        // console.log("Todo form: ", values);
        const { onSubmit } = props;
        if (onSubmit)
            await onSubmit(values)
        form.reset()
    }
    const { isSubmitting } = form.formState
    return (
        <div>
            {isSubmitting && <LinearProgress />}
            <Avatar>
                <LockClockOutlined></LockClockOutlined>
            </Avatar>
            <Typography component="h3" variant="h5">
                Login
            </Typography>
            <form onSubmit=
                {form.handleSubmit(handleSubmit)}>


                <InputField
                    name="identifier"
                    label="Email"
                    form={form} />
                <PasswordField
                    name="password"
                    label="Password"
                    form={form} />

                <Button
                    disabled={isSubmitting}
                    variant='contained'
                    color='primary'
                    fullWidth
                    type='submit'
                >
                    Login
                </Button>
            </form>
        </div>
    );
}

export default LoginForm;