import { login } from 'features/Auth/userSlide';
import { useSnackbar } from 'notistack';
import React from 'react';
import { useDispatch } from 'react-redux';
import LoginForm from '../LoginForm';

Login.propTypes = {

};

function Login(props) {

    const dispatch = useDispatch()
    const { enqueueSnackbar } = useSnackbar();
    const handleSubmit = async (values) => {

        try {
            const action = login(values)
            const user
                = await dispatch(action).unwrap()
            const { closeDialog } = props
            if (closeDialog) {
                closeDialog()
            }




        } catch (error) {
            console.log('Failed to login:', error);
        }
    }
    return (
        <div>
            <LoginForm onSubmit={handleSubmit} />
        </div>
    );
}

export default Login;