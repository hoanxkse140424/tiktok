import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx'

import styles from './TodoList.module.scss'



TodoList.propTypes = {
    todoList: PropTypes.array,
    onTodoClick: PropTypes.func,
};
TodoList.defaultProps = {
    todoList: [],
    onTodoClick: null,
}

function TodoList({ todoList, onTodoClick }) {
    // console.log(todoList);
    const handleTodoList = (todo, index) => {

        if (!onTodoClick) return;
        onTodoClick(todo, index)
    }
    return (

        <ul className={styles.todoList}>
            {
                todoList.map((todo, index) => (
                    <li key={todo.id}
                        className={clsx(
                            { [styles.completed]: todo.status === 'completed' }
                        )}
                        onClick={() => {
                            handleTodoList(todo, index)

                        }}
                    >
                        {todo.title}
                    </li>
                ))
            }
        </ul >

    );
}

export default TodoList;