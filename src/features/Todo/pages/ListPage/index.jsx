import React, { useEffect, useMemo, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import queryString from 'query-string'
import TodoList from '../../components/TodoList';
import TodoForm from '../../components/TodoForm';

ListPage.propTypes = {

};

function ListPage(props) {
    const initTodoList = [
        {
            id: 1,
            title: 'Eat',
            status: 'new'
        },
        {
            id: 2,
            title: 'Sleep',
            status: 'completed'
        },
        {
            id: 3,
            title: 'Study',
            status: 'new'
        }
    ]
    const location = useLocation();
    const navigate = useNavigate();

    const [todoList, setTodoList] = useState(initTodoList)

    const [filteredStatus, setFilterStatus]
        = useState(() => {
            const params = queryString.parse(location.search);
            return params.status || 'all'
        })

    useEffect(() => {
        const params = queryString.parse(location.search);
        setFilterStatus(params.status || 'all')
    }, [location.search])
    const handleTodoClick = (todo, index) => {

        //clone current array to the new one
        const newTodoList = [...todoList];
        for (let i = 0; i < newTodoList.length; i++) {
            if (newTodoList[i].id === todo.id) {
                newTodoList[i] = {
                    ...newTodoList[i],
                    status: newTodoList[i].status === 'new' ? 'completed' : 'new'
                }
            }

        }

        //  todo.status === 'new' ? 'completed' : 'new' 

        setTodoList(newTodoList)





    }
    const handleShowAllClick = () => {

        // setFilterStatus('all')
        const queryParams = { status: 'all' }
        navigate({
            pathname: location.pathname,
            search: queryString.stringify(queryParams)
        })
    }
    const handleShowCompletedClick = () => {
        // setFilterStatus('completed')
        const queryParams = { status: 'completed' }
        navigate({
            pathname: location.pathname,
            search: queryString.stringify(queryParams)
        })
    }
    const handleShowNewClick = () => {
        // setFilterStatus('new')
        const queryParams = { status: 'new' }
        navigate({
            pathname: location.pathname,
            search: queryString.stringify(queryParams)
        })
    }

    const renderedTodoList =
        useMemo(
            () => {
                return todoList.filter(todo =>
                    filteredStatus === 'all' ||
                    filteredStatus === todo.status)
            }, [todoList, filteredStatus]
        )



    const handleTodoFormSubmit = (values) => {
        console.log("Form submit: ", values);
        const newTodo = {
            id: todoList.length + 1,
            title: values.title,
            status: 'new'

        }
        const newTodoList = [...todoList, newTodo]
        setTodoList(newTodoList)
    }
    return (
        <div>
            <h3>What to do</h3>
            <TodoForm onSubmit={handleTodoFormSubmit} />

            <h3>Todo List</h3>
            <TodoList
                todoList={renderedTodoList}

                onTodoClick={handleTodoClick}
            />
            <div>
                <button
                    onClick={handleShowAllClick}
                >
                    Show All
                </button>
                <button
                    onClick={handleShowCompletedClick}
                >
                    Show Completed
                </button>
                <button
                    onClick={handleShowNewClick}
                >
                    Show New
                </button>
            </div>
        </div>
    );
}

export default ListPage;