import React from 'react';
import { Route, Routes } from 'react-router-dom';
import NotFound from '../../components/NotFound';
import DetailPage from './pages/DetailPage';
import ListPage from './pages/ListPage';

TodoFeature.propTypes = {

};

function TodoFeature(props) {

    return (


        <div>
            <h1>
                To do Shared UI
            </h1>
            <Routes>

                <Route path="" element={<ListPage />}></Route>
                <Route path=":todoId" element={<DetailPage />}></Route>
                <Route path="*" element={<NotFound />} />
            </Routes>
        </div>

    );
}

export default TodoFeature;