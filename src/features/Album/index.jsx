import React from 'react';
import PropTypes from 'prop-types';
import AlbumList from './components/AlbumList';

AlbumFeature.propTypes = {

};

function AlbumFeature(props) {
    const albumList = [
        {
            id: 1,
            name: 'Think Of You',
            thumbnailUrl: 'https://photo-resize-zmp3.zadn.vn/w240_r1x1_jpeg/cover/9/a/8/e/9a8ee776b32a291fc9fdd36208bd4452.jpg'
        },
        {
            id: 2,
            name: 'Mẹ ơi! Tết Van Dặm Xa',
            thumbnailUrl: 'https://photo-resize-zmp3.zadn.vn/w240_r1x1_jpeg/cover/9/1/e/8/91e8004178d4a9abe28a17a988b9a80e.jpg'
        },
        {
            id: 3,
            name: 'Về (Đi Để Trở Về 6)',
            thumbnailUrl: 'https://photo-resize-zmp3.zadn.vn/w240_r1x1_jpeg/cover/5/6/6/f/566f0774c0358db7d903bed9f5d6bfe1.jpg'
        }
    ]
    return (
        <div>
            <h2>Album Hot</h2>
            <AlbumList albumList={albumList} />
        </div>
    );
}

export default AlbumFeature;