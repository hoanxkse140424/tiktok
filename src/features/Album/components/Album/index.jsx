import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import styles from './Album.module.scss'

Album.propTypes = {
    album: PropTypes.object.isRequired
};

function Album({ album }) {
    return (
        <div className={clsx(styles.album)}>
            <div className={clsx(styles.albumThumbnail)}>
                <img src={album.thumbnailUrl} alt={album.name} />
            </div>
            <p className={clsx(styles.albumName)}>{album.name}</p>
        </div>
    );
}

export default Album;