import Button from '@mui/material/Button';
import Header from 'components/Header';
import { useSnackbar } from 'notistack';
import { useEffect } from 'react';
import { NavLink, Route, Routes } from "react-router-dom";
import productApi from './api/productApi';
import NotFound from "./components/NotFound";
import AlbumFeature from "./features/Album";
import CounterFeature from './features/Counter';
import TodoFeature from "./features/Todo";




function App() {

  useEffect(() => {
    const fetchProducts = async () => {
      const params = {
        _limit: 10
      }
      const productList =
        await productApi.getAll(params)
      console.log(productList);
    }
    fetchProducts();
  }, [])



  return (
    <div >
      <Header />


      <Button variant="contained">Hello World</Button>;
      {/* <p><Link to="/todos">Todos</Link></p>
      <p><Link to="/albums">Albums</Link></p> */}





      <Routes>
        <Route path="/" element={<CounterFeature />} />
        <Route path="todos/*" element={<TodoFeature />} />
        <Route path="albums" element={<AlbumFeature />} />

        <Route path="*" element={<NotFound />} />
      </Routes>


    </div>
  )
}
export default App;
